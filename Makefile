CFLAGS += -O2 -g
CFLAGS += -std=gnu99 -Wall -Werror
CFLAGS += -pthread
CFLAGS += -DCONFIG_KPROBES
#CFLAGS += -DCONFIG_HAVE_ARCH_PS_CONST
INCLUDES = -I include/ -I arch/x86/include/
CFLAGS += $(INCLUDES)

CFLAGS +=-Wl,-Tarch/x86/kernel/vmlinux.lds

ifeq ($(V),1)
	Q=
	NQ=@true
else
	Q=@
	NQ=@echo
endif

all: arch/x86/kernel/vmlinux.lds main parse-bzimage

arch/x86/kernel/vmlinux.lds: arch/x86/kernel/vmlinux.lds.S
	$(NQ) '  LD  ' $@
	$(Q)$(CC) $(CFLAGS) $(INCLUDES) -E -P \
		-D__ASSEMBLY__ -DLINKER_SCRIPT -o $@ $<

%.o: %.c *.h
	$(NQ) '  CC  ' $@
	$(Q)$(CC) -c $(CPPFLAGS) $(CFLAGS) -o $@ $<

parse-bzimage: parse-bzimage.c
	$(NQ) '  CC  ' $@
	$(Q)$(CC) -o $@ $<

# Simply placing alpha before beta will ensure
# alpha runs prior to beta. This is expected given
# SORT() for the init tables work on the order level
# defined only:
#
# *(SORT(SECTION_TBL_ALL(SECTION_INIT_DATA)))
#
# The name of the struct places no effect to the actual linker order.
OBJS =  \
	arch/x86/kernel/head64.c \
	kasan.o\
	memory.o \
	arch/x86/kernel/alternative.o \
	arch/x86/kernel/init.o \
	kernel/locking/mutex.o \
	kernel/locking/spinlock.o \
	kernel/workqueue.o \
	kernel/main.o \
	kprobes.o \
	pci.o \
	pci-quirks.o \
	beta.o \
	alpha.o \
	drivers/acme.o \
	drivers/synth/main.o \
	drivers/synth/common.o \
	drivers/synth/or.S \
	xen.o \
	xen-driver.o \
	main.o

main: $(OBJS)
	$(NQ) '  CC  ' $@
	$(Q)$(CC) $(CPPFLAGS) $(CFLAGS) -o $@ $^

clean:
	find ./ -name \*.o | xargs rm -f
	rm -f main parse-bzimage *.o arch/x86/kernel/vmlinux.lds
