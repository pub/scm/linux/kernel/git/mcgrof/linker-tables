#ifndef _ASM_GENERIC_SECTIONS_H_
#define _ASM_GENERIC_SECTIONS_H_

#ifndef __ASSEMBLY__

#include <linux/sections.h>

#ifdef __ASSEMBLER__

#ifndef SECTION_TYPE
#define SECTION_TYPE(section, type, name, level)		\
	push_section_type(section, type, name, level, )
#endif

/* Used on foo.S for instance */
#ifndef push_section_type
#define push_section_type(section, type, name, level, flags)			\
	.pushsection section.type.name.#level, #flags
#endif

#else /* __ASSEMBLER__ */

/* Used in C code, for instance, on asm volatile() code. */

#ifndef __push_section_type
#define __push_section_type(section, type, name, level, flags)			\
	".pushsection " #section "." #type "." #name "." #level ",  \"" #flags "\"\n"

#endif /* __ASSEMBLER__ */
#endif /* __ASSEMBLY__ */

#ifndef push_section_type
#define push_section_type(section, type, name, level, flags)			\
	__push_section_type(section, type, name, level)
#endif

#endif

#endif /* _ASM_GENERIC_SECTIONS_H_ */
