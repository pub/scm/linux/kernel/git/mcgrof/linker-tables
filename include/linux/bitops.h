#ifndef _LINUX_BITOPS_H
#define _LINUX_BITOPS_H

#define BIT(nr)		(1UL << (nr))

#endif /* _LINUX_BITOPS_H */
