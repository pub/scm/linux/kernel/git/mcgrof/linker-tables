#ifndef _LINUX_RANGES_H
#define _LINUX_RANGES_H

#include <linux/sections.h>

#define SECTION_TYPE_RANGES	rng

#define SECTION_RANGE(section, name, level)				\
	SECTION_TYPE(section, SECTION_TYPE_RANGES, name, level)

#ifdef __ASSEMBLER__
#define push_section_rng(section, name, level, flags)			\
	push_section_type(section, SECTION_TYPE_RANGES, name, level, flags)

#define DECLARE_SECTION_RANGE(section, name)				\
  push_section_rng(section, name,,) ;					\
  .globl name ;								\
name: ;									\
  .popsection								\
									\
  push_section_rng(section, name, ~,) ;					\
  .popsection

#else

#ifndef __ASSEMBLY__

/* Linker tables shalt not use this */
#define SECTION_ADDR_IN_RANGE(name, addr)				\
	 (addr >= (unsigned long) LINUX_SECTION_START(name) &&		\
          addr <  (unsigned long) LINUX_SECTION_END(name))

#define DECLARE_SECTION_RANGE(name)					\
	DECLARE_LINUX_SECTION_RO(char, name)

#define SECTION_RANGE_BEGIN(name, __section)				\
	const __typeof__(name[0])					\
	      __attribute__((used,					\
			     weak,					\
			     __aligned__(LINUX_SECTION_ALIGNMENT(name)),\
			     section(SECTION_RANGE(__section, name, ))))

#define SECTION_RANGE_END(name, __section)				\
	const __typeof__(name[0])					\
	      __attribute__((used,					\
			     __aligned__(LINUX_SECTION_ALIGNMENT(name)),\
			     section(SECTION_RANGE(__section, name, ~))))

#define DEFINE_SECTION_RANGE(name, section)				\
	DECLARE_LINUX_SECTION_RO(char, name);				\
	SECTION_RANGE_BEGIN(name, section) VMLINUX_SYMBOL(name)[0] = {};\
	LTO_REFERENCE_INITCALL(name);					\
	SECTION_RANGE_END(name, section) VMLINUX_SYMBOL(name##__end)[0] = {};\
	LTO_REFERENCE_INITCALL(name##__end);

#define __LINUX_RANGE(section, name)					\
	__attribute__((__aligned__(LINUX_SECTION_ALIGNMENT(name)),	\
		       __section__(SECTION_RANGE(section, name, SECTION_ORDER_ANY))))

#define __LINUX_RANGE_ORDER(section, name, level)			\
	__attribute__((__aligned__(LINUX_SECTION_ALIGNMENT(name)),	\
		       __section__(SECTION_RANGE(section, name, level))))

#define push_section_rng(section, name, level)				\
	push_section_type(section, SECTION_TYPE_RANGES, name, level)

#endif /* __ASSEMBLY__ */
#endif /* __ASSEMBLER__ */

#define SECTION_RANGE_ALL(section)					\
	SECTION_TYPE_ALL(section,SECTION_TYPE_RANGES)


#endif /* _LINUX_RANGES_H */
