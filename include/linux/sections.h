#ifndef _LINUX_SECTIONS_H
#define _LINUX_SECTIONS_H

#include <asm/sections.h>

#define SECTION_RODATA			.rodata
#define SECTION_TEXT			.text
#define SECTION_DATA			.data
#define SECTION_INIT			.init.text
#define SECTION_INIT_DATA		.init.data
#define SECTION_INIT_RODATA		.init.rodata
#define SECTION_INIT_CALL		.initcall
#define SECTION_EXIT			.exit.text
#define SECTION_EXIT_DATA		.exit.data
#define SECTION_EXIT_CALL		.exitcall.exit
#define SECTION_REF			.ref.text
#define SECTION_REF_DATA		.ref.data
#define SECTION_REF_RODATA		.ref.rodata
#define SECTION_SCHED			.sched.text

#ifndef __ASSEMBLER__

#define LINUX_SECTION_ALIGNMENT(name)	__alignof__(*name)
#define LINUX_SECTION_SIZE(name)	((name##__end) - (name))
#define LINUX_SECTION_EMPTY(name)	(LINUX_RANGE_SIZE(name) == 0)
#define LINUX_SECTION_START(name)	name
#define LINUX_SECTION_END(name)	name##__end

#define LINUX_SECTION(name, section)					\
	#section "." #name

#define DECLARE_LINUX_SECTION(type, name)				\
	 extern type name[], name##__end[];

#define DECLARE_LINUX_SECTION_RO(type, name)				\
	 extern const type name[], name##__end[];

#define __SECTION_TYPE(section, type, name, level)			\
	#section "." #type "." #name "." #level

#define SECTION_TYPE(section, type, name, level)                       	\
	__SECTION_TYPE(section, type, name, level)

#define SECTION_ORDER_ANY	any

#endif /* __ASSEMBLER__ */

/*
 * This section is for use on linker scripts and helpers
 */
#define SECTION_TYPE_ALL(section, type)					\
	section##.##type.*
#endif /* _LINUX_SECTIONS_H */
